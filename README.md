Place Auto Complete Form
===================

# Description
Place auto complete form that use the google API. It is possible to have an inline
form or a complete form with multiple input (street number, city, etc).
It is also possible to specify the type of the suggestion (city, country, etc).

The suggestions are in the language of the app (it uses `$translate.use()` to get 
the current language) and there are geolocated.

# Install

    bower install git@gitlab.com:rdl-bower-components/directive-placeAutoCompleteForm.git --save

Then add `rdl.placeAutoCompleteForm` in your index.module.js

# Exemple
```html
<rdl-place-auto-complete-form
    api-key="AIzaSyD7hClRu3A-jf0EabWV9HIPVSSHM-4QD7E"
    id="address"
    location="ctrl.location"
    enter-address-text="location.enterAddress"
    street-address-text="location.streetAddress"
    city-text="location.city"
    state-text="location.state"
    zip-code-text="location.zipCode"
    county-text="location.county"
    search-text="location.search"
    disabled="!ctrl.editable">
</rdl-place-auto-complete-form>
```

# Directive attributes

## id (`required`)

The unique identifier of the form.

## api-key (`required`)

The google API key to use. Go to https://console.developers.google.com to get it.

## location (`required`)

An object where the result of the search is put.
The structure of the object is like : 
```javascript
{
    address: '20 Rue de la Part-Dieu, Lyon, France',
    street_number: '20',
    route: 'Rue de la Part-Dieu',
    locality: 'Lyon',
    administrative_area_level_1: 'Auvergne Rhône-Alpes',
    country: 'France',
    postal_code: '69003',
    geo : {
        lon : 4.8427070000000185,
        lat : 45.75915759999999
    }
}
```

## only-search-input

If present, only the search input will be present. The other fields (city, country, zip code...)
will be hidden.

## restrict-type

See https://developers.google.com/places/web-service/autocomplete?hl=fr#place_types for more information.
By default the type is `geocode`.

## disabled

If `true`, the form will be disabled. By default, the value is `false`.

## required

If `true`, the main input will have the `required` attribute. By default, the value is `false`.

## city-text

The text of the "city" field - will be translated.
By default the text is `location.city`.

## county-text

The text of the "country" field - will be translated.
By default the text is `location.county`.

## enter-address-text

The text of the placeholder  - will be translated.
By default the text is `location.enterAddress`.

## search-text

The text of the "search" field - will be translated.
By default the text is `location.search`.

## state-text

The text of the "state" field - will be translated.
By default the text is `location.state`.

## street-address-text

The text of the "street address" field - will be translated.
By default the text is `location.streetAddress`.

## zip-code-text

The text of the "zip code" field - will be translated.
By default the text is `location.zipCode`.
