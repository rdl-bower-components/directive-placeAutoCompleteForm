/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	exports.PlaceAutoCompleteFormDirective = PlaceAutoCompleteFormDirective;

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	angular.module('rdl.placeAutoCompleteForm', ['angularLoad']).directive('rdlPlaceAutoCompleteForm', PlaceAutoCompleteFormDirective);

	function PlaceAutoCompleteFormDirective() {
	  'ngInject';

	  var directive = {
	    restrict: 'E',
	    replace: true,
	    template: '<ng-form name="placeAutoCompleteForm" class="form-horizontal place-auto-complete-form"><div class="form-group" ng-class="{\'inline-auto-complete\' : vm.onlySearchInput !== undefined}"><label ng-if="vm.onlySearchInput === undefined" for="{{vm.idInput}}" class="col-xs-2 control-label" translate>{{vm.searchText}}</label><div ng-class="(vm.onlySearchInput === undefined) ? \'col-xs-10\' : \'col-xs-12 inline-auto-complete\'"><input id="{{vm.idInput}}" class="form-control" placeholder="{{vm.enterAddressText | translate}}" ng-focus="vm.geolocate()" type="text" ng-disabled="vm.disabled" ng-model="vm.location.address" ng-required="vm.required"></div></div><div ng-if="vm.onlySearchInput === undefined"><div class="form-group"><label for="inputStreetNumber" class="col-xs-2 control-label" translate>{{vm.streetAddressText}}</label><div class="col-xs-3"><input type="text" class="form-control" id="inputStreetNumber" ng-model="vm.location.street_number" ng-disabled="vm.pristine"></div><div class="col-xs-7"><input type="text" class="form-control" id="inputStreetAddress" ng-model="vm.location.route" ng-disabled="vm.pristine"></div></div><div class="form-group"><label for="inputCity" class="col-xs-2 control-label" translate>{{vm.cityText}}</label><div class="col-xs-10"><input type="text" class="form-control" id="inputCity" ng-model="vm.location.locality" ng-disabled="vm.pristine"></div></div><div class="form-group"><label for="inputState" class="col-xs-2 control-label" translate>{{vm.stateText}}</label><div class="col-xs-4"><input type="text" class="form-control" id="inputState" ng-model="vm.location.administrative_area_level_1" ng-disabled="vm.pristine"></div><label for="inputZipCode" class="col-xs-2 control-label" translate>{{vm.zipCodeText}}</label><div class="col-xs-4"><input type="text" class="form-control" id="inputZipCode" ng-model="vm.location.postal_code" ng-disabled="vm.pristine"></div></div><div class="form-group"><label for="inputCounty" class="col-xs-2 control-label" translate>{{vm.countyText}}</label><div class="col-xs-10"><input type="text" class="form-control" id="inputCounty" name="inputCounty" ng-model="vm.location.country" ng-disabled="vm.pristine"></div></div></div></ng-form>',
	    scope: {
	      id: '@',
	      apiKey: '@',
	      location: '=',
	      enterAddressText: "@?",
	      streetAddressText: '@?',
	      cityText: '@?',
	      stateText: '@?',
	      zipCodeText: '@?',
	      countyText: '@?',
	      searchText: '@?',
	      onlySearchInput: '@?',
	      restrictType: '@?',
	      disabled: '=?',
	      required: '=?'

	    },
	    controller: PlaceAutoCompleteFormController,
	    controllerAs: 'vm',
	    bindToController: true
	  };

	  return directive;
	}

	var PlaceAutoCompleteFormController = function () {
	  PlaceAutoCompleteFormController.$inject = ["angularLoad", "$scope", "$timeout", "$translate"];
	  function PlaceAutoCompleteFormController(angularLoad, $scope, $timeout, $translate) {
	    'ngInject';

	    var _this = this;

	    _classCallCheck(this, PlaceAutoCompleteFormController);

	    this.$scope = $scope;
	    this.$timeout = $timeout;

	    this.idInput = this.id + "-input";
	    this.disabled = this.disabled ? this.disabled : false;
	    this.required = this.required ? this.required : false;
	    this.restrictType = this.restrictType ? this.restrictType : 'geocode';
	    this.enterAddressText = this.enterAddressText ? this.enterAddressText : 'location.enterAddress';
	    this.streetAddressText = this.streetAddressText ? this.streetAddressText : 'location.streetAddress';
	    this.cityText = this.cityText ? this.cityText : 'location.city';
	    this.stateText = this.stateText ? this.stateText : 'location.state';
	    this.zipCodeText = this.zipCodeText ? this.zipCodeText : 'location.zipCode';
	    this.countyText = this.countyText ? this.countyText : 'location.county';
	    this.searchText = this.searchText ? this.searchText : 'location.search';
	    this.pristine = true;

	    this.componentForm = {
	      street_number: 'short_name',
	      route: 'long_name',
	      locality: 'long_name',
	      administrative_area_level_1: 'short_name',
	      country: 'long_name',
	      postal_code: 'short_name'
	    };

	    angularLoad.loadScript('https://maps.googleapis.com/maps/api/js?key=' + this.apiKey + '&signed_in=true&libraries=places&language=' + $translate.use()).then(function () {
	      _this.$timeout(function () {
	        return _this.initAutocomplete();
	      });
	    });
	  }

	  _createClass(PlaceAutoCompleteFormController, [{
	    key: 'initAutocomplete',
	    value: function initAutocomplete() {
	      var _this2 = this;

	      this.autocomplete = new google.maps.places.Autocomplete(document.getElementById(this.idInput), { types: [this.restrictType] });
	      this.autocomplete.addListener('place_changed', function () {
	        return _this2.fillInAddress();
	      });

	      if (this.location !== undefined && this.location.address !== undefined) {
	        document.getElementById(this.idInput).value = this.location.address;
	      }
	    }
	  }, {
	    key: 'fillInAddress',
	    value: function fillInAddress() {
	      var _this3 = this;

	      var place = this.autocomplete.getPlace();

	      this.$scope.$apply(function () {
	        _this3.location = {};
	        _this3.pristine = false;

	        for (var i = 0; i < place.address_components.length; i++) {
	          var addressType = place.address_components[i].types[0];
	          if (_this3.componentForm[addressType]) {
	            _this3.location[addressType] = place.address_components[i][_this3.componentForm[addressType]];
	          }
	        }
	        _this3.location.geo = {
	          lat: place.geometry.location.lat(),
	          lon: place.geometry.location.lng()
	        };

	        _this3.location.address = document.getElementById(_this3.idInput).value;
	      });
	    }
	  }, {
	    key: 'geolocate',
	    value: function geolocate() {
	      var autocomplete = this.autocomplete;
	      if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(function (position) {
	          var geolocation = {
	            lat: position.coords.latitude,
	            lng: position.coords.longitude
	          };
	          var circle = new google.maps.Circle({
	            center: geolocation,
	            radius: position.coords.accuracy
	          });
	          autocomplete.setBounds(circle.getBounds());
	        });
	      }
	    }
	  }]);

	  return PlaceAutoCompleteFormController;
	}();

/***/ }
/******/ ]);