angular.module('rdl.placeAutoCompleteForm',
    [
        'angularLoad'
    ])
    .directive('rdlPlaceAutoCompleteForm', PlaceAutoCompleteFormDirective);

export function PlaceAutoCompleteFormDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    replace: true,
    templateUrl: 'placeAutoCompleteForm.html',
    scope: {
      id: '@',
      apiKey: '@',
      location: '=',
      enterAddressText: "@?",
      streetAddressText: '@?',
      cityText: '@?',
      stateText: '@?',
      zipCodeText: '@?',
      countyText: '@?',
      searchText: '@?',
      onlySearchInput: '@?',
      restrictType: '@?',
      disabled: '=?',
      required: '=?'

    },
    controller: PlaceAutoCompleteFormController,
    controllerAs: 'vm',
    bindToController: true
  };

  return directive;
}

class PlaceAutoCompleteFormController {
  constructor (angularLoad, $scope, $timeout, $translate) {
    'ngInject';

    this.$scope = $scope;
    this.$timeout = $timeout;

    this.idInput = this.id + "-input";
    this.disabled = (this.disabled) ? this.disabled : false;
    this.required = (this.required) ? this.required : false;
    this.restrictType = (this.restrictType) ? this.restrictType : 'geocode';
    this.enterAddressText = (this.enterAddressText) ? this.enterAddressText : 'location.enterAddress';
    this.streetAddressText = (this.streetAddressText) ? this.streetAddressText : 'location.streetAddress';
    this.cityText = (this.cityText) ? this.cityText : 'location.city';
    this.stateText = (this.stateText) ? this.stateText : 'location.state';
    this.zipCodeText = (this.zipCodeText) ? this.zipCodeText : 'location.zipCode';
    this.countyText = (this.countyText) ? this.countyText : 'location.county';
    this.searchText = (this.searchText) ? this.searchText : 'location.search';
    this.pristine = true;

    this.componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      locality: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
    };

    angularLoad.loadScript('https://maps.googleapis.com/maps/api/js?key=' + this.apiKey + '&signed_in=true&libraries=places&language=' + $translate.use()).then(() => {
      this.$timeout(() => this.initAutocomplete());
    });
  }
  initAutocomplete() {
    this.autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById(this.idInput)),
        {types: [this.restrictType]});
    this.autocomplete.addListener('place_changed', () => this.fillInAddress());

    if (this.location !== undefined && this.location.address !== undefined) {
      document.getElementById(this.idInput).value = this.location.address;
    }
  }

  fillInAddress() {
    var place = this.autocomplete.getPlace();

    this.$scope.$apply(() => {
      this.location = {};
      this.pristine = false;

      for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (this.componentForm[addressType]) {
          this.location[addressType] = place.address_components[i][this.componentForm[addressType]];
        }
      }
      this.location.geo = {
        lat: place.geometry.location.lat(),
        lon: place.geometry.location.lng()
      };

      this.location.address = document.getElementById(this.idInput).value;
    });
  }

  geolocate() {
    var autocomplete = this.autocomplete;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var geolocation = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        var circle = new google.maps.Circle({
          center: geolocation,
          radius: position.coords.accuracy
        });
        autocomplete.setBounds(circle.getBounds());
      });
    }
  }
}